package Animals;

import World.Organism;
import World.World;

import java.awt.*;
import java.util.Random;

public abstract class Animal extends Organism {
    public Animal() {
        super();
    }

    @Override
    public void action() {
        Random rand = new Random();

        int x = rand.nextInt(3) - 1;
        int y = rand.nextInt(3) - 1;

        x = checkBoundaries(x + this.getxPosition());
        y = checkBoundaries(y + this.getyPosition());

        Organism oponent = this.getWorld().getOrganismsManager().getOrganismByXY(x, y);

        if (oponent == null) {
            this.setxPosition(x);
            this.setyPosition(y);
        } else if (oponent != this) {
            oponent.collision(this);
        }
    }

    public abstract Color getColor();

    @Override
    public void collision(Organism oponent) {
        if (oponent.getSign() == this.getSign()) {
            this.multiply(oponent);
        } else if (oponent.getPower() < this.getPower()) {
            this.getWorld().getOrganismsManager().deleteOrganism(oponent);

        } else {
            oponent.setxPosition(this.getxPosition());
            oponent.setyPosition(this.getyPosition());
            oponent.getWorld().getOrganismsManager().deleteOrganism(this);
        }
    }

    public void multiply(Organism org2) {
        Organism newOne;

        for (int x = -2; x < 2; x++) {
            for (int y = -1; y < 2; y++) {
                x = checkBoundaries(x + this.getxPosition());
                y = checkBoundaries(y + org2.getyPosition());

                Organism oponent = this.getWorld().getOrganismsManager().getOrganismByXY(x, y);

                if (oponent == null || (oponent.getPower() < this.getPower())) {
                    this.getWorld().getOrganismsManager().addOrganism(this.bornMe(this.getWorld(), x, y));
                    x = 2;
                    break;
                } else if (oponent == org2) {
                    continue;
                }

            }
        }
    }

    @Override
    public abstract Animal bornMe(World world, int x, int y);
}
