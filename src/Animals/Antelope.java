package Animals;

import World.Organism;
import World.World;

import java.awt.*;
import java.util.Random;

public class Antelope extends Animal {
    public static Color color = new Color(198, 166, 100);

    public Antelope(World world, int x, int y) {
        this.setPower(4);
        this.setInitiative(4);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('A');
        this.setWorld(world);
        this.setDeathLog("Antylopa zginela");
    }

    public Color getColor() {
        return Antelope.color;
    }

    @Override
    public final void action() {
        Random random = new Random();

        int x = random.nextInt(5) - 2;  //zasieg ruchu
        int y = random.nextInt(5) - 2;


        //FIXME petla z az znajdzie wolne miejsce oraz bool czy jest dookola wolne miejsce
        x = checkBoundaries(x + this.getxPosition());
        y = checkBoundaries(y + this.getyPosition());

        Organism oponent = this.getWorld().getOrganismsManager().getOrganismByXY(x, y);

        if (oponent == null) {
            this.setyPosition(y);
            this.setxPosition(x);
        } else {
            oponent.collision(this);
        }
    }

    @Override
    public Animal bornMe(World world, int x, int y) {
        return new Antelope(world, x, y);
    }

    @Override
    public final void collision(Organism oponent) {
        Random random = new Random();

        if (oponent.getSign() == this.getSign()) {
            this.multiply(oponent);
        }

        //prawdopodobienstwo 50% na ucieczke
        if (random.nextInt(100) % 2 == 0) {
            int x = this.getxPosition();
            int y = this.getyPosition();

            do {
                this.action();
            } while (x == this.getxPosition() && y == this.getyPosition()); //sprawdzenie czy nastapila zmiana pozycji
        } else {
            oponent.setxPosition(this.getxPosition());
            oponent.setyPosition(this.getyPosition());
            oponent.getWorld().getOrganismsManager().deleteOrganism(this);
        }


    }
}
