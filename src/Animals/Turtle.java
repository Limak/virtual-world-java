package Animals;

import World.Organism;
import World.World;

import java.awt.*;
import java.util.Random;

public class Turtle extends Animal {

    public static Color color = new Color(32, 113, 179);

    public Turtle(World world, int x, int y) {
        this.setPower(2);
        this.setInitiative(1);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('T');
        this.setWorld(world);
        this.setDeathLog("Żółw zginął");
    }

    public Color getColor() {
        return Turtle.color;
    }

    @Override
    public void action() {
        Random random = new Random();

        //25% szans na przesuniecie sie
        if (random.nextInt(4) == 0) {
            super.action();
        }
    }

    @Override
    public void collision(Organism oponent) {

        //FIXME zrobic comparator
        if (oponent.getSign() == this.getSign()) {
            this.multiply(oponent);
        } else if (oponent.getPower() > 5) {   //FIXME co znaczy 5
            oponent.setxPosition(this.getxPosition());
            oponent.setyPosition(this.getyPosition());
            this.getWorld().getOrganismsManager().deleteOrganism(this);
        }
    }

    @Override
    public Animal bornMe(World world, int x, int y) {
        return new Turtle(world, x, y);
    }
}
