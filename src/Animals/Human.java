package Animals;

import World.Organism;
import World.World;

import java.awt.*;

public class Human extends Animal {

    public static Color color = new Color(184, 183, 153);
    private int nextX;
    private int nextY;
    private boolean shouldWorkingTimer;
    private int tmpPower;
    private int additionalSuperPower;
    private int superPowerWaitCounter;

    public Human(World world) {
        this.setxPosition(0);
        this.setyPosition(0);
        this.setInitiative(4);
        this.setPower(5);
        shouldWorkingTimer = false;
        this.setAge(0);
        this.setSign('H');
        this.setWorld(world);
        this.setDeathLog("Czlowiek zostal zabity ");
    }

    public Color getColor() {
        return Human.color;
    }

    @Override
    public void collision(Organism oponent) {
        super.collision(oponent);
    }

    @Override
    public Animal bornMe(World world, int x, int y) {
        return null;
    }

    @Override
    public void action() {
        int x = this.getxPosition();
        int y = this.getyPosition();

        x = checkBoundaries(this.getxPosition() + this.nextX);
        y = checkBoundaries(this.getyPosition() + this.nextY);

        Organism enemy = this.getWorld().getOrganismsManager().getOrganismByXY(x, y);

        if (enemy == null) {
            this.setxPosition(x);
            this.setyPosition(y);
        } else {
            enemy.collision(this);
        }

        /*if(this.superPowerWaitCounter==0 && !this.shouldWorkingTimer) {
            this.additionalSuperPower=5;
            this.tmpPower=this.getPower();
            this.setPower(this.getPower()+additionalSuperPower);
            this.shouldWorkingTimer=true;
        }*/
    }

    public void checkUsingSuperPower() {
        if (this.additionalSuperPower > 0) {
            additionalSuperPower--;
            //power--;
            this.setPower(getPower() - 1);
        }
    /*else if (this->additionalSuperPower == 0 && superPowerWaitCounter == 0)
	{
		this->tmpPower = power;
		this->additionalSuperPower = 5;
	}*/
        else if (superPowerWaitCounter == 0 && this.getPower() == tmpPower && shouldWorkingTimer) {
            this.setSuperPowerWaitCounter(5);   //ustawienie liczninika do super mocy
            shouldWorkingTimer = false;
        } else if (this.superPowerWaitCounter > 0) superPowerWaitCounter--;
        else {
            superPowerWaitCounter = 0;
            shouldWorkingTimer = false;
        }


    }

    public void setNextX(int nextX) {
        this.nextX = nextX;
    }

    public void setNextY(int nextY) {
        this.nextY = nextY;
    }

    public boolean isShouldWorkingTimer() {
        return shouldWorkingTimer;
    }

    public void setShouldWorkingTimer(boolean shouldWorkingTimer) {
        this.shouldWorkingTimer = shouldWorkingTimer;
    }

    public int getAdditionalSuperPower() {
        return additionalSuperPower;
    }

    public void setAdditionalSuperPower(int additionalSuperPower) {
        this.additionalSuperPower = additionalSuperPower;
    }

    public int getTmpPower() {
        return tmpPower;
    }

    public boolean getShouldWorkingTimer() {
        return shouldWorkingTimer;
    }

    public void setTmpPower(int tmpPower) {
        this.tmpPower = tmpPower;
    }

    public int getSuperPowerWaitCounter() {
        return superPowerWaitCounter;
    }

    public void setSuperPowerWaitCounter(int superPowerWaitCounter) {
        this.superPowerWaitCounter = superPowerWaitCounter;
    }
}
