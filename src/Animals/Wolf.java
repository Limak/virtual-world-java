package Animals;

import World.World;

import java.awt.*;


public class Wolf extends Animal {

    public static Color color = new Color(0, 0, 0);

    public Wolf(World world, int x, int y) {
        this.setPower(9);
        this.setInitiative(5);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('W');
        this.setWorld(world);
        this.setDeathLog("Wilk nie żyje");
    }

    public Color getColor() {
        return Wolf.color;
    }

    @Override
    public Animal bornMe(World world, int x, int y) {
        return new Wolf(world, x, y);
    }
}
