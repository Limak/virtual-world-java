package Animals;

import World.Organism;
import World.World;

import java.awt.*;
import java.util.Random;

public class Fox extends Animal {

    public static Color color = new Color(255, 117, 20);

    public Fox(World world, int x, int y) {
        this.setPower(3);
        this.setInitiative(7);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('F');
        this.setWorld(world);
        this.setDeathLog("Lis zginal");
    }

    public Color getColor() {
        return Fox.color;
    }

    @Override
    public void action() {
        Random random = new Random();

        int x = random.nextInt(2) - 1;
        int y = random.nextInt(2) - 1;

        x = checkBoundaries(x + this.getxPosition());
        y = checkBoundaries(y + this.getyPosition());

        Organism enemy = this.getWorld().getOrganismsManager().getOrganismByXY(x, y);

        if (enemy == null) {
            this.setxPosition(x);
            this.setyPosition(y);
        } else if (enemy.getPower() < this.getPower()) {
            enemy.collision(this);
        }
    }

    @Override
    public Animal bornMe(World world, int x, int y) {
        return new Fox(world, x, y);
    }
}
