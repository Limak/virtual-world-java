package Animals;

import World.World;

import java.awt.*;

public class Sheep extends Animal {

    public static Color color = new Color(255, 255, 204);

    public Sheep(World world, int x, int y) {
        this.setPower(4);
        this.setInitiative(4);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('O');
        this.setWorld(world);
        this.setDeathLog("Owca zgineła ");
    }

    public Color getColor() {
        return Sheep.color;
    }

    @Override
    public Animal bornMe(World world, int x, int y) {
        return new Sheep(world, x, y);
    }
}
