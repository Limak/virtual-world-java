package Plants;

import Animals.Animal;
import World.Organism;
import World.World;

import java.awt.*;

public class SosnowskiBarszcz extends Plant {

    public static Color color = new Color(159, 230, 0);

    public SosnowskiBarszcz(World world, int x, int y) {
        this.setPower(10);
        this.setInitiative(0);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('b');
        this.setWorld(world);
        this.setDeathLog("Barscz został zjedzony");
    }

    public Color getColor() {
        return SosnowskiBarszcz.color;
    }

    @Override
    public void collision(Organism enemy) {
        this.getWorld().getOrganismsManager().deleteOrganism(enemy);
    }


    //zabijanie wszystkich dookola
    @Override
    public void action() {

        for (int y = -1; y < 2; y++) {
            for (int x = -1; x < 2; x++) {


                //TODO sprawdzic czy sa na planszy
                //czy X i Y sa poprawne oraz czy to pole jest puste
                //jesli jest to usunac go
                Organism comparedOrganism = getWorld().getOrganismsManager().getOrganismByXY(getxPosition() + x, getyPosition() + y);

                //FIXME
                if (comparedOrganism != null) {
                    if (inBoundaries(comparedOrganism.getxPosition() + x, comparedOrganism.getyPosition() + y) && !isEmptyPlace(comparedOrganism.getxPosition() + x, comparedOrganism.getyPosition() + y)) {

                        if (comparedOrganism instanceof Animal)
                            this.getWorld().getOrganismsManager().deleteOrganism(comparedOrganism);
                    }
                } else continue;
            }
        }
    }

    @Override
    public Plant bornMe(World world, int x, int y) {
        return new SosnowskiBarszcz(world, x, y);
    }
}
