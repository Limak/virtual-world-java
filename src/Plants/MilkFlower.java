package Plants;

import World.World;

import java.awt.*;

public class MilkFlower extends Plant {

    public static Color color = new Color(255, 255, 0);

    public MilkFlower(World world, int x, int y) {
        this.setPower(0);
        this.setInitiative(0);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('m');
        this.setWorld(world);
        this.setDeathLog("mlecz został zjedzony ");
    }

    public Color getColor() {
        return MilkFlower.color;
    }

    @Override
    public void action() {
        for (int i = 0; i < 3; i++) {
            super.action(); //porusza sie sie 3 razy
        }
    }

    @Override
    public Plant bornMe(World world, int x, int y) {
        return new MilkFlower(world, x, y);
    }
}
