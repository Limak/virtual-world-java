package Plants;

import World.Organism;
import World.World;

import java.awt.*;

public class WolfBerries extends Plant {

    public static Color color = new Color(160, 52, 114);

    public WolfBerries(World world, int x, int y) {
        this.setPower(99);
        this.setInitiative(0);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('&');
        this.setWorld(world);
        this.setDeathLog("Jagody zostały zjedzone ");
    }

    public Color getColor() {
        return WolfBerries.color;
    }

    @Override
    public void collision(Organism enemy) {
        this.getWorld().getOrganismsManager().deleteOrganism(enemy);
        this.getWorld().getOrganismsManager().deleteOrganism(this);
    }

    @Override
    public Organism bornMe(World world, int x, int y) {
        return new WolfBerries(world, x, y);
    }
}
