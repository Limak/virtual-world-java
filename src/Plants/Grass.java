package Plants;

import World.World;

import java.awt.*;

public class Grass extends Plant {
    public static Color color = new Color(53, 134, 45);

    public Grass(World world, int x, int y) {
        this.setPower(0);
        this.setInitiative(0);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('g');
        this.setWorld(world);
        this.setDeathLog("Trawa została zjedzona ");
    }

    public Color getColor() {
        return Grass.color;
    }

    @Override
    public Plant bornMe(World world, int x, int y) {
        return new Grass(world, x, y);
    }
}
