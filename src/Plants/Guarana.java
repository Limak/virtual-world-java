package Plants;

import Animals.Animal;
import World.Organism;
import World.World;

import java.awt.*;

public class Guarana extends Plant {

    public static Color color = new Color(230, 50, 68);

    public Guarana(World world, int x, int y) {
        this.setPower(0);
        this.setInitiative(0);
        this.setxPosition(x);
        this.setyPosition(y);
        this.setSign('@');
        this.setWorld(world);
        this.setDeathLog("Guarana została zjedzona");
    }

    public Color getColor() {
        return Guarana.color;
    }

    @Override
    public final void collision(Organism oponent) {
        if (oponent instanceof Animal) {
            oponent.setPower(oponent.getPower() + 3);
            oponent.setxPosition(this.getxPosition());
            oponent.setyPosition(this.getyPosition());
            this.getWorld().getOrganismsManager().deleteOrganism(this);
        }

    }


    @Override
    public Plant bornMe(World world, int x, int y) {
        return new Guarana(world, x, y);
    }
}
