package Plants;

import World.Organism;
import World.World;

import java.util.Random;

public abstract class Plant extends Organism {

    public Plant() {
        super();
    }

    @Override
    public void action() {
        Random random = new Random();

        //20% szans na rozprzestrzenienie sie rosliny
        if (random.nextInt(5) == 0) {
            int x = random.nextInt(3) - 1;    //zakres maxymalnych przesuniec
            int y = random.nextInt(3) - 1;

            x = checkBoundaries(x + this.getxPosition());
            y = checkBoundaries(y + this.getyPosition());

            Organism oponenet = this.getWorld().getOrganismsManager().getOrganismByXY(x, y);

            if (oponenet == null) {
                Plant newPlant = (Plant) this.bornMe(this.getWorld(), x, y);
                this.getWorld().getOrganismsManager().addOrganism(newPlant);
            }
        }
    }

    @Override
    public void collision(Organism enemy) {
        enemy.setxPosition(this.getxPosition());
        enemy.setyPosition(this.getyPosition());
        this.getWorld().getOrganismsManager().deleteOrganism(this);
    }

    @Override
    public abstract Organism bornMe(World world, int x, int y);
}
