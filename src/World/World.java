package World;

import Animals.*;
import Frames.MyFrame;
import Plants.*;

import java.io.*;
import java.util.Collections;

/**
 * Created by kamil on 02.05.2017.
 */
public class World {
    private static String fileName = "save.txt";
    private final int width = 20;
    private final int height = 20;
    private MyFrame frame;
    private Character map[][] = new Character[height][width];
    private String log;
    private OrganismsManger organismsManager = new OrganismsManger();
    private int roundCounter;
    private int maxAmountOfObjects = 400;


    public Character[][] getMap() {
        return map;
    }

    public MyFrame getFrame() {
        return frame;
    }

    public void setFrame(MyFrame frame) {
        this.frame = frame;
    }

    public OrganismsManger getOrganismsManager() {
        return organismsManager;
    }

    public int getRoundCounter() {
        return roundCounter;
    }

    public void setLog(String log) {
        this.log += log;
    }

    public void saveWorld() throws IOException {
        PrintWriter file = null;

        try {
            file = new PrintWriter(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        file.println(this.roundCounter);
        file.println(this.organismsManager.getSize());

        for (Organism organism : this.organismsManager.organisms) {
            if (organism != null) {
                writeToFile(file, organism);

                if(organism.getSign()=='H')
                {
                    file.println(((Human) organism).getAdditionalSuperPower());
                    file.println(((Human)organism).getSuperPowerWaitCounter());
                    file.println(((Human)organism).getShouldWorkingTimer());
                    file.println(((Human)organism).getTmpPower());
                }
            }

        }
        file.close();
    }

    public void loadWorld() throws IOException {
        FileReader file = new FileReader("save.txt");
        BufferedReader reader = new BufferedReader(file);

        this.organismsManager.clearArrayOfOrganisms();
        this.roundCounter = Integer.valueOf(reader.readLine());
        int tempSize = Integer.valueOf(reader.readLine());
        char tempIndicator;
        int tempStrength, tempAge, tempX, tempY;
        Organism newOne;


        for (int i = 0; i < tempSize; i++) {
            tempIndicator = reader.readLine().charAt(0);
            tempStrength = Integer.valueOf(reader.readLine());
            tempAge = Integer.valueOf(reader.readLine());
            tempX = Integer.valueOf(reader.readLine());
            tempY = Integer.valueOf(reader.readLine());

            switch (tempIndicator) {
                case 'A':
                    newOne = new Antelope(this, tempX, tempY);
                    break;

                case 'F':
                    newOne = new Fox(this, tempX, tempY);
                    break;
                case 'O':
                    newOne = new Sheep(this, tempX, tempY);
                    break;
                case 'W':
                    newOne = new Wolf(this, tempX, tempY);
                    break;
                case 'T':
                    newOne = new Turtle(this, tempX, tempY);
                    break;
                case '@':
                    newOne = new Guarana(this, tempX, tempY);
                    break;
                case 'm':
                    newOne = new MilkFlower(this, tempX, tempY);
                    break;
                case 'g':
                    newOne = new Grass(this, tempX, tempY);
                    break;
                case '&':
                    newOne = new WolfBerries(this, tempX, tempY);
                    break;
                case 'b':
                    newOne = new SosnowskiBarszcz(this, tempX, tempY);
                    break;
                case 'H': {
                    newOne = new Human(this);
                    newOne.setxPosition(tempX);
                    newOne.setyPosition(tempY);
                    int tempSuperPower = Integer.valueOf(reader.readLine());
                    ((Human)newOne).setAdditionalSuperPower(tempSuperPower);
                    int tempStarnSuperPowerRound = Integer.valueOf(reader.readLine());
                    ((Human)newOne).setSuperPowerWaitCounter(tempStarnSuperPowerRound);
                    boolean sholuldWorkingTimer=Boolean.valueOf(reader.readLine());
                    ((Human)newOne).setShouldWorkingTimer(sholuldWorkingTimer);
                    int tmpPower = Integer.valueOf(reader.readLine());
                    ((Human)newOne).setSuperPowerWaitCounter(tmpPower);
                    ((Human)newOne).setTmpPower(tmpPower);
                    break;
                }
                default:
                    newOne = new MilkFlower(this, tempX, tempY);
            }
            newOne.setAge(tempAge);
            newOne.setPower(tempStrength);
            this.organismsManager.addOrganism(newOne);

        }
        this.fillMap();
    }

    private void clearMap() {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                map[i][j] = ' ';
            }
        }
    }

    public void nextRound() {
        Collections.sort(this.organismsManager.organisms, new OrganismComparator());
        this.log = "";

        if (findHuman() != null) {
            findHuman().checkUsingSuperPower();
        }

        int size = this.organismsManager.getSize();

        if (size >= maxAmountOfObjects) {
            System.exit(0);
        }


        for (int i = 0; i < size; i++) {
            Organism current = this.organismsManager.getOrganism(i);

            if (current != null) {
                current.setAge(current.getAge() + 1);
                current.action();
            }

            if (this.organismsManager.getSize() < size)       //gdy coś zostanie zjedzone
                size = this.organismsManager.getSize();
        }
        this.roundCounter++;
        this.fillMap();
    }

    public Human findHuman() {
        for (Organism organism : this.organismsManager.organisms) {
            if (organism != null && organism.getSign() == 'H') {
                return (Human) organism;
            }
        }
        return null;
    }

    public void fillMap() {
        clearMap();
        int x = 0, y = 0;

        for (int i = 0; i < this.organismsManager.getSize(); i++) {
            Organism organism = this.organismsManager.getOrganism(i);
            if (organism != null) {
                x = organism.getxPosition();
                y = organism.getyPosition();

                if (x < 20 && x >= 0 && y < 20 && y >= 0) map[y][x] = organism.getSign();
            }
        }
    }

    public String getLogs() {
        return log;
    }

    public void initialise() {

        this.organismsManager.addOrganism(new Guarana(this, 1, 5));
        this.organismsManager.addOrganism(new SosnowskiBarszcz(this, 5, 7));
        this.organismsManager.addOrganism(new Fox(this, 9, 18));
        this.organismsManager.addOrganism(new MilkFlower(this, 6, 15));
        this.organismsManager.addOrganism(new Grass(this, 8, 11));
        this.organismsManager.addOrganism(new Wolf(this, 10, 2));
        this.organismsManager.addOrganism(new WolfBerries(this, 4, 15));
        this.organismsManager.addOrganism(new Turtle(this, 17, 9));
        this.organismsManager.addOrganism(new Sheep(this, 4, 15));
        this.organismsManager.addOrganism(new Antelope(this, 11, 19));
        this.organismsManager.addOrganism(new Human(this));

        this.fillMap();
    }

    private void writeToFile(PrintWriter file, Organism organism) {
        file.println(organism.getSign());
        file.println(organism.getPower());
        file.println(organism.getAge());
        file.println(organism.getxPosition());
        file.println(organism.getyPosition());
    }
}
