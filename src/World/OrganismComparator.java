package World;

import java.util.Comparator;

public class OrganismComparator implements Comparator<Organism> {
    @Override
    public int compare(Organism o1, Organism o2) {
        if (o1 != null && o2 != null) {
            if (o1.getInitiative() < o2.getInitiative()) {
                return 1;
            } else if (o1.getInitiative() == o2.getInitiative()) {
                if (o1.getAge() < o2.getAge()) {
                    return 1;
                } else if (o1.getAge() > o2.getAge()) return -1;
                else return 0;
            } else {
                return -1;
            }
        } else return 0;

    }
}