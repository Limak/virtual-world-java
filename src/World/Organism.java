package World;

import java.awt.*;

public abstract class Organism {
    private int power;
    private int initiative;
    private int xPosition;
    private int yPosition;
    private int age;
    private char sign;
    private World world;
    private String deathLog;

    public Organism(World world, int x, int y) {
        this.setxPosition(x);
        this.setyPosition(y);
        this.setWorld(world);
    }

    public Organism() {
        this.setxPosition(0);
        this.setyPosition(0);
        this.setAge(0);
        this.setInitiative(0);
        this.setPower(0);
        this.setSign('0');
        this.setWorld(new World());
    }

    public abstract Color getColor();

    public abstract void action();

    public abstract void collision(Organism enemy);

    public abstract Organism bornMe(World world, int x, int y);


    public int checkBoundaries(int n) {
        final int size = 20;

        for (int i = 0; i < 2; i++)
            if (n >= size) {
                n -= 1;
            }

        for (int i = 0; i < 2; i++)
            if (n < 0) {
                n += 1;
            }
        return n;
    }

    public boolean isEmptyPlace(int organismX, int organismY) {

        if (!inBoundaries(organismX, organismY)) return false;
        else if (world.getOrganismsManager().getOrganismByXY(organismX, organismY) == null) return true;
        return false;
    }

    public int getyPosition() {
        return yPosition;
    }

    public void setyPosition(int yPosition) {
        this.yPosition = yPosition;
    }

    public String getDeathLog() {
        return deathLog;
    }

    public void setDeathLog(String deathLog) {
        this.deathLog = deathLog;
    }

    public int getxPosition() {
        return xPosition;
    }

    public void setxPosition(int xPosition) {
        this.xPosition = xPosition;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getSign() {
        return sign;
    }

    public void setSign(char sign) {
        this.sign = sign;
    }

    public int getInitiative() {
        return initiative;
    }

    public void setInitiative(int initiative) {
        this.initiative = initiative;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public boolean inBoundaries(int x, int y) {
        final int size = 20;
        if (x >= 0 && x < size && y >= 0 && y < size) return true;
        return false;
    }

    public String infoAboutDead(Organism dead) {


        switch (dead.getSign()) {
            case 'H': {
                return dead.getDeathLog();
            }
            case 'A': {
                return dead.getDeathLog();
            }
            case 'F': {
                return dead.getDeathLog();
            }
            case 'O': {
                return dead.getDeathLog();
            }
            case 'T': {
                return dead.getDeathLog();
            }
            case '&': {
                return dead.getDeathLog();
            }
            case 'g': {
                return dead.getDeathLog();
            }
            case 'm': {
                return dead.getDeathLog();
            }
            case '@': {
                return dead.getDeathLog();
            }
            default: {
                return " ";
            }
        }
    }


}
