package World;

import java.util.ArrayList;

/**
 * Created by kamil on 02.05.2017.
 */
public class OrganismsManger {
    protected ArrayList<Organism> organisms = new ArrayList<Organism>();

    public Organism getOrganismByXY(int x, int y) {
        for (int i = 0; i < this.getSize(); i++) {
            if (this.organisms.get(i) != null && this.organisms.get(i).getxPosition() == x && this.organisms.get(i).getyPosition() == y) {
                return this.organisms.get(i);
            }
        }
        return null;
    }

    public int getSize() {
        return organisms.size();
    }

    public void addOrganism(Organism organismToAdd) {
        this.organisms.add(organismToAdd);
    }

    public void clearArrayOfOrganisms() {
        this.organisms.clear();
    }

    //usuwa organizm z listy organizmow
    public void deleteOrganism(Organism organismToDelete) {
        organismToDelete.getWorld().setLog(organismToDelete.infoAboutDead(organismToDelete));
        this.organisms.remove(organismToDelete);
    }

    public Organism getOrganism(int x) {
        return this.organisms.get(x);
    }
}


