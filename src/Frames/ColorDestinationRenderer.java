package Frames;

import Animals.*;
import Plants.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class ColorDestinationRenderer extends DefaultTableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object val, boolean selected, boolean focused, int row, int col) {
        Component comp = super.getTableCellRendererComponent(table, val, selected, focused, row, col);
        char organismSign = (char) val;

        switch (organismSign) {
            case 'H': {
                comp.setBackground(Human.color);
                break;
            }
            case 'A': {
                comp.setBackground(Antelope.color);
                break;
            }
            case 'F': {
                comp.setBackground(Fox.color);
                break;
            }
            case 'O': {
                comp.setBackground(Sheep.color);
                break;
            }
            case 'T': {
                comp.setBackground(Turtle.color);
                break;
            }
            case 'W': {
                comp.setBackground(Wolf.color);
                break;
            }
            case 'm': {
                comp.setBackground(MilkFlower.color);
                break;
            }
            case 'g': {
                comp.setBackground(Grass.color);
                break;
            }
            case '&': {
                comp.setBackground(WolfBerries.color);
                break;
            }
            case '@': {
                comp.setBackground(Guarana.color);
                break;
            }
            case 'b': {
                comp.setBackground(SosnowskiBarszcz.color);
                break;
            }
            default: {
                comp.setBackground(new Color(189, 236, 182));
                break;
            }
        }
        comp.setForeground(comp.getBackground());
        return comp;
    }
}
