package Frames;

import Animals.Human;
import World.World;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import static java.awt.event.KeyEvent.*;

/**
 * Created by kamil on 03.05.2017.
 */
public class MyFrame extends JFrame implements KeyListener, MouseListener {

    protected World frameWorld;
    protected AddingFrame addingFrame;
    private JButton nextRoundButton;
    private JButton saveButton;
    private JButton openButton;
    private JButton superPowerButton;
    private JTable table;
    private JLabel roundNumber;
    private JLabel humanPowerLabel;
    private JLabel superPowerTimerLabel;
    private JLabel logs;


    public MyFrame(World world) {
        super("Kamil Mastalerz 165296");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(500, 200);
        setSize(800, 500);
        setResizable(true);
        setVisible(true);
        this.frameWorld = world;
        world.setFrame(this);

        Object names[] = {' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ',};

        int btnSize = 150;

        table = new JTable(this.frameWorld.getMap(), names) {
            public boolean isCellEditable(int rowIndex, int colIndex)   //blokowanie edycji komórek
            {
                return false;
            }
        };


        table.setDefaultRenderer(Object.class, new ColorDestinationRenderer());       //kolorowanie komórek zgodnie z zawartością
        table.setSize(500, 320);
        table.setGridColor(new Color(189, 236, 182));
        table.hasFocus();
        table.addKeyListener(this);
        table.addMouseListener(this);
        add(table);

        nextRoundButton = new JButton("Następna runda");
        nextRoundButton.setLocation(10, 350);
        nextRoundButton.setSize(btnSize, 30);
        nextRoundButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Human human = frameWorld.findHuman();
                if (human != null) {
                    human.setNextX(0);      //do zakomentowania w razie zmiany sterowania
                    human.setNextY(0);
                }

                frameWorld.nextRound();
                frameWorld.getFrame().repaint();
                table.grabFocus();
                roundNumber.setText(String.valueOf("Runda: " + frameWorld.getRoundCounter()));

                if (frameWorld.findHuman() != null) {
                    humanPowerLabel.setText(String.valueOf("Moc czlowieka: " + frameWorld.findHuman().getPower()));
                    superPowerTimerLabel.setText(String.valueOf("Czas do supermocy: " + frameWorld.findHuman().getSuperPowerWaitCounter()));
                }


                logs.setText(frameWorld.getLogs());
            }
        });

        saveButton = new JButton("Zapisz gre");
        saveButton.setLocation(10, 380);
        saveButton.setSize(btnSize, 30);
        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    frameWorld.saveWorld();
                    table.grabFocus();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });


        openButton = new JButton("Otwórz grę");
        openButton.setLocation(10, 410);
        openButton.setSize(btnSize, 30);
        openButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    frameWorld.loadWorld();
                    frameWorld.getFrame().repaint();
                    table.grabFocus();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });


        superPowerButton = new JButton("Supermoc");
        superPowerButton.setLocation(250, 350);
        superPowerButton.setSize(btnSize, 30);
        superPowerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (frameWorld.findHuman().getSuperPowerWaitCounter() == 0 && !frameWorld.findHuman().isShouldWorkingTimer())    //sprawdzenie czy czas oczekiwania wynosi 0
                {
                    frameWorld.findHuman().setAdditionalSuperPower(5);
                    frameWorld.findHuman().setTmpPower(frameWorld.findHuman().getPower());
                    frameWorld.findHuman().setPower(frameWorld.findHuman().getPower() + frameWorld.findHuman().getAdditionalSuperPower());
                    frameWorld.findHuman().setShouldWorkingTimer(true);
                    System.out.println(frameWorld.findHuman().getPower());
                }
                table.grabFocus();
            }
        });


        add(nextRoundButton);
        add(saveButton);
        add(openButton);
        add(superPowerButton);


        if (frameWorld.findHuman() != null) {
            humanPowerLabel = new JLabel();
            humanPowerLabel.setText("Moc człowieka: " + String.valueOf(frameWorld.findHuman().getPower()));
            System.out.println(String.valueOf(frameWorld.findHuman().getPower()));
            humanPowerLabel.setLocation(500, 10);
            humanPowerLabel.setSize(150, 30);
            add(humanPowerLabel);


            superPowerTimerLabel = new JLabel();
            superPowerTimerLabel.setText("Czas do supermocy: " + String.valueOf(frameWorld.findHuman().getSuperPowerWaitCounter()));
            superPowerTimerLabel.setLocation(500, 45);
            superPowerTimerLabel.setSize(150, 30);
            add(superPowerTimerLabel);
        }


        roundNumber = new JLabel();
        roundNumber.setText("Runda: " + String.valueOf(frameWorld.getRoundCounter()));
        roundNumber.setLocation(250, 380);
        roundNumber.setSize(200, 30);
        add(roundNumber);


        logs = new JLabel();
        logs.setText(frameWorld.getLogs());
        logs.setLocation(250, 410);
        logs.setSize(400, 30);
        add(logs);

        setLayout(new GroupLayout(table));
        setLayout(null);
        addingFrame = new AddingFrame(this);
        addingFrame.setVisible(false);
        addingFrame.parentFrame = this;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        Human human = this.frameWorld.findHuman();
        int key = e.getKeyCode();

        if (human != null) {
            switch (key) {
                case VK_UP: {
                    human.setNextY(-1);
                    human.setNextX(0);
                    break;
                }
                case VK_DOWN: {
                    human.setNextY(1);
                    human.setNextX(0);
                    break;
                }
                case VK_LEFT: {
                    human.setNextX(-1);
                    human.setNextY(0);
                    break;
                }
                case VK_RIGHT: {
                    human.setNextX(1);
                    human.setNextY(0);
                    break;
                }
                default: {
                    human.setNextX(0);
                    human.setNextY(0);
                    break;
                }
            }
        }

        this.hasFocus();
        this.frameWorld.nextRound();
        this.frameWorld.getFrame().repaint();
        roundNumber.setText(String.valueOf("Runda: " + frameWorld.getRoundCounter()));

        if (frameWorld.findHuman() != null) {
            humanPowerLabel.setText("Moc człowieka: " + String.valueOf(frameWorld.findHuman().getPower()));
            superPowerTimerLabel.setText("Czas do supermocy: " + String.valueOf(frameWorld.findHuman().getSuperPowerWaitCounter()));
        }

        logs.setText(frameWorld.getLogs());
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int row = table.getSelectedRow();
        int col = table.getSelectedColumn();

        this.addingFrame.readPlaceForNewOrganism(row, col);
        this.addingFrame.hasFocus();
        this.addingFrame.setVisible(true);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public World getFrameWorld() {
        return this.frameWorld;
    }
}




