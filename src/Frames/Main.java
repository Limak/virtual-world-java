package Frames;

import World.World;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        World world = new World();
        world.initialise();
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                world.setFrame(new MyFrame(world));
            }
        });
    }
}
