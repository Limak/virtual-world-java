package Frames;

import Animals.*;
import Plants.*;
import World.Organism;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddingFrame extends JFrame implements ActionListener {

    protected MyFrame parentFrame;
    protected AddingButton antelopeButton = new AddingButton("Antylopa");
    protected AddingButton foxButton = new AddingButton("Lis");
    protected AddingButton sheepButton = new AddingButton("Owca");
    protected AddingButton turtleButton = new AddingButton("Żółw");
    protected AddingButton wolfButton = new AddingButton("Wilk");
    protected AddingButton berryButton = new AddingButton("Wilcza Jagoda");
    protected AddingButton milkFlowerButton = new AddingButton("Mlecz");
    protected AddingButton grassButton = new AddingButton("Trawa");
    protected AddingButton guaranaButton = new AddingButton("Guarana");
    protected AddingButton barszczButton = new AddingButton("Barszcz Sosnowski");

    private int newOrganismX;
    private int newOrganismY;

    public AddingFrame(MyFrame parent) {
        super("Wybierz organizm, który chcesz dodać");
        this.parentFrame = parent;
        setLayout(new GridLayout());
        setLocation(400, 300);
        setSize(900, 80);
        setResizable(false);
        setVisible(false);


        antelopeButton.setBackground(new Color(198, 166, 100));
        foxButton.setBackground(new Color(255, 117, 20));
        sheepButton.setBackground(Color.WHITE);
        turtleButton.setBackground(new Color(34, 113, 179));
        wolfButton.setBackground(Color.BLACK);
        berryButton.setBackground(new Color(160, 52, 114));
        milkFlowerButton.setBackground(Color.YELLOW);
        grassButton.setBackground(new Color(53, 104, 45));
        guaranaButton.setBackground(new Color(230, 50, 68));
        barszczButton.setBackground(new Color(159, 230, 0));

        add(antelopeButton);
        add(foxButton);
        add(sheepButton);
        add(turtleButton);
        add(wolfButton);
        add(berryButton);
        add(milkFlowerButton);
        add(grassButton);
        add(guaranaButton);
        add(barszczButton);

        antelopeButton.addActionListener(this);
        foxButton.addActionListener(this);
        sheepButton.addActionListener(this);
        turtleButton.addActionListener(this);
        wolfButton.addActionListener(this);
        berryButton.addActionListener(this);
        milkFlowerButton.addActionListener(this);
        grassButton.addActionListener(this);
        guaranaButton.addActionListener(this);
        barszczButton.addActionListener(this);

    }

    public void readPlaceForNewOrganism(int row, int col) {
        this.newOrganismX = col;
        this.newOrganismY = row;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();
        Organism newOne;

        if (parentFrame.frameWorld.getOrganismsManager().getOrganismByXY(this.newOrganismX, this.newOrganismY) != null) {
            parentFrame.frameWorld.getOrganismsManager().deleteOrganism(parentFrame.frameWorld.
                    getOrganismsManager().getOrganismByXY(this.newOrganismX, this.newOrganismY));
        }

        if (source == antelopeButton) {
            newOne = new Antelope(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else if (source == foxButton) {
            newOne = new Fox(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else if (source == sheepButton) {
            newOne = new Sheep(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else if (source == turtleButton) {
            newOne = new Turtle(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else if (source == wolfButton) {
            newOne = new Wolf(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else if (source == berryButton) {
            newOne = new WolfBerries(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else if (source == grassButton) {
            newOne = new Grass(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else if (source == guaranaButton) {
            newOne = new Guarana(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else if (source == barszczButton) {
            newOne = new SosnowskiBarszcz(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        } else {
            newOne = new MilkFlower(this.parentFrame.frameWorld, this.newOrganismX, this.newOrganismY);
        }
        this.parentFrame.frameWorld.getOrganismsManager().addOrganism(newOne);
        this.parentFrame.frameWorld.fillMap();
        this.parentFrame.repaint();
        this.dispose();

    }

}


class AddingButton extends JButton {
    AddingButton(String name) {
        super(name);
        setBorderPainted(false);
        setFocusPainted(true);
    }

}
